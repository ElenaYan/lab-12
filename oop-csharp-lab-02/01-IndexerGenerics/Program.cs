﻿using System;
using System.Linq;

namespace IndexerGenerics
{
    class Program
    {
        /*
         * == README ==
         * 
         * Il progetto consiste in una libreria che fornisce l'astrazione di mappa bidimensionale
         * (cioè mappa con due chiavi). Il progetto fornisce l'interfaccia IMap2D<TKey1, TKey2, TValue>, 
         * che mostra i metodi forniti da una mappa bi-dimensionale, e la classe Program che rappresenta 
         * il punto d'ingresso del programma di test. 
         * 
         * Le implementazioni dell'interfaccia IMap2D permettono di memorizzare valori di tipo TValue
         * in quella che può essere pensata come una matrice sparsa, in cui le righe sono indicizzate
         * con valori di tipo TKey1 e le colonne con valori di tipo TKey2. 
         * 
         * Dato un oggetto IMap<TKey1, TKey2, TValue> map e due chiavi k1 di tipo TKey1 e k2 di tipo TKey2,
         * si accede al valore corrispondente con la sintassi map[k1, k2].
         * 
         * Scopo dell'esercizio è implementare la classe Map2D<TKey1, TKey2, TValue>. 
         * 
         * Il test contenuto nella classe Program tenta di chiarire il comportamento atteso per una classe
         * che implementi IMap2D. Esso lancia eccezioni nel caso di errata implementazione della classe Map2D.
         */

        static void Main(string[] args)
        {
            IMap2D<int, int, int> pitagoricTable = new Map2D<int, int, int>();

            pitagoricTable.Fill(Enumerable.Range(1, 10), Enumerable.Range(1, 10), (i, j) => i * j);

            Console.WriteLine(pitagoricTable);

            //for (int i = 1; i <= 10; i++)
            //{
            //    if (pitagoricTable[i, i] != i * i)
            //    {
            //        throw new Exception("Wrong implementation");
            //    }
            //}

            //int[] seven = new int[] { 7, 14, 21, 28, 35, 42, 49, 56, 63, 70 };

            //if (!pitagoricTable.GetRow(7).Select(t => t.Item2).SequenceEqual(seven))
            //{
            //    throw new Exception("Wrong implementation");
            //}

            //if (!pitagoricTable.GetColumn(7).Select(t => t.Item2).SequenceEqual(seven))
            //{
            //    throw new Exception("Wrong implementation");
            //}
        }
    }
}
